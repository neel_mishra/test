def shrink(string,font_size):

    #Number of characters
    string = str(string)
    max_size = 25
    upper_medium_size = 20
    medium = 15
    lower_medium = 10

    red_max = 10
    red_small = 5
    med_factor = 0
    inc_factor_med = 5
    inc_factor_large = 10
    large = 15

    if ( len(string) >= max_size):

        return font_size - red_max

    elif (len(string) >= upper_medium_size):

        return font_size - red_small

    elif (len(string) >= medium):
    
        return font_size - med_factor


    elif(len(string) < medium):

        return font_size + inc_factor_med

    elif(len(string) < lower_medium):
        
        return font_size + inc_factor_large

    else :

         return font_size + large
