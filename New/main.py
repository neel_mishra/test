import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class print_button:
    def onDeleteWindow(self, *args):
        Gtk.main_quit(*args)

    def onButtonPressed(self, button):
        print("Hello World!")

builder = Gtk.Builder()
builder.add_from_file("source.glade")
builder.connect_signals(print_button())

window = builder.get_object("main_window")
window.show_all()

Gtk.main()
