import shrink

def paint(canvas, location , size, font, text):
        try:
            int(text)
            canvas.setFont(font, size)
            canvas.drawString(location[0],location[1], str(text))
        
        except:
            print("Text found executing exception", " The text is", text)
            size = shrink.shrink(text,size)
            canvas.setFont(font, size)
            print(text)
            canvas.drawString(location[0],location[1], text)


def pdf(info):

    from PyPDF2 import PdfFileWriter, PdfFileReader
    import io
    from reportlab.pdfgen import canvas
    from reportlab.lib.pagesizes import letter,A4
    
    w,h = A4
    #1
    n_id = 0
    n_loc = (430,342)
    n_size = 36
    n_font = 'Courier'
    #6
    d_id = 5
    d_loc = (120, 311.5)
    d_size = 36
    d_font = 'Courier'
    #2
    e_id = 1
    e_loc = (120, 282)
    e_size = 32
    e_font = 'Courier'
    #7
    s_id = 6
    s_loc = (w,2)
    s_size = 20
    s_font = 'Courier'
    
    
    

    existing_pdf = PdfFileReader(open("design.pdf", "rb"))
    
    packet = io.BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=A4)
    
    #Itreative section
    
    for detail in info:
        paint(can, n_loc, n_size, n_font, detail[n_id])
        paint(can, d_loc, d_size, d_font, detail[d_id])
        paint(can, e_loc, e_size, e_font, detail[e_id])
        paint(can, s_loc, s_size, s_font, detail[s_id])
        can.showPage()
    #Itreative portion ends
    
    class temp_pdf():
        
        def get_page(self,page_no):
            self.outputStream.close()
            self.reader = PdfFileReader(self.name+'.pdf', 'rb')
            page = self.reader.getPage(page_no + 1)
            self.outputStream = open(self.name, 'wb')
            return page
        
        def d_page(self,page):
            self.output.addPage(page)
            self.output.write(self.outputStream)
        
        def __init__(self,filename):
            self.name = filename
            self.outputStream = open(self.name + '.pdf', "wb")
            self.output = PdfFileWriter()
            self.output.addBlankPage(h,w)
            self.output.write(self.outputStream)
            
    
    can.save()
    
    #move to the beginning of the StringIO buffer
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    # read your existing PDF
    
    output = PdfFileWriter()
    
    a = temp_pdf('temp')
    # add the "watermark" (which is the new pdf) on the existing page
    
    page = existing_pdf.getPage(0)
    a.d_page(page)
    
    i = 0
    for pages in new_pdf.pages:    
        
        dpage = a.get_page(0)
        dpage.mergePage(new_pdf.getPage(i))
        output.addPage(dpage)
        del(dpage)
        i += 1
        print('Step Complete')
        

    outputStream = open("destination.pdf", "wb")
    
    print("Certificates created and merged = ", i )
    
    output.write(outputStream)
    outputStream.close()